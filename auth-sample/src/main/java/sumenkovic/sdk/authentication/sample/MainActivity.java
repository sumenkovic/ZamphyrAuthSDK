package sumenkovic.sdk.authentication.sample;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import sumenkovic.sdk.authentication.Authentication;
import sumenkovic.sdk.authentication.ChromeCustomTab;


public class MainActivity extends AppCompatActivity {

    public static final String CLIENT_ID = "tJqsvch4X8";
    public static final String CLIENT_SECRET = "YuB3Dwmxrj26DPHEhCTY";
    public static final String GRANT_TYPE = "authorization_code";
    public static final String REDIRECT_URI = "zamph://callback";
    public static final String STATE = "randomString123";

    private String mainToken;
    private TextView code;
    private EditText token;
    private String userInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Authentication.openLoginActivity(this, CLIENT_ID, CLIENT_SECRET, REDIRECT_URI, STATE);

        code = findViewById(R.id.accessCode);
        token = findViewById(R.id.accessToken);
    }

    public void onAddZamphyrAccountClicked(View view) {
        if (mainToken == null)
            Toast.makeText(getApplicationContext(), "You have to request user first!", Toast.LENGTH_SHORT).show();
        else {
            AccountManager accountManager = AccountManager.get(MainActivity.this); //this is Activity
            final Account account = new Account(userInfo, "sumenkovic.sdk.authentication.sample");

            boolean success = accountManager.addAccountExplicitly(account, Authentication.token.getAccessToken(), null);

            if (success)
                Toast.makeText(getApplicationContext(), userInfo + " account added successfully", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(getApplicationContext(), userInfo + " account already exists or fails. Try removing it first.", Toast.LENGTH_SHORT).show();
        }
    }

    public void onRemoveZamphyrAccountClicked(View view) {
        AccountManager accountManager = AccountManager.get(MainActivity.this); //this is Activity
        Account[] accounts = accountManager.getAccounts();
        for (int index = 0; index < accounts.length; index++) {
            if (accounts[index].type.intern() == "sumenkovic.sdk.authentication.sample")
                accountManager.removeAccount(accounts[index], null, null);
        }
        Toast.makeText(getApplicationContext(), "All Zamphyr accounts removed successfully! ", Toast.LENGTH_SHORT).show();
    }

    public void onRequestUserInfoCLicked(View view) {
        if (mainToken == null)
            Toast.makeText(getApplicationContext(), userInfo + "You have to login and obtain token first!", Toast.LENGTH_SHORT).show();
        else {
            final String url = "https://api.zamphyr.com/me?access_token=" + Authentication.token.getAccessToken();
            RequestQueue queue = Volley.newRequestQueue(this);
            StringRequest getRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                Gson gson = new GsonBuilder().create();
                                JsonObject job = gson.fromJson(response, JsonObject.class);
                                JsonElement name = job.getAsJsonObject("data").getAsJsonPrimitive("name");
                                userInfo = URLDecoder.decode(URLEncoder.encode(name.getAsString(), "iso8859-1"), "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }

                            Log.i("User info Response", response.toString());

                            final TextView user = findViewById(R.id.userInfo);
                            user.setText(userInfo.toString());
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("Error.Response", error.toString());
                            //userInfo.setText(error.toString());
                        }
                    }
            );

            // add it to the RequestQueue
            queue.add(getRequest);
        }
    }

    public void onLoginClicked(View view) {
        Authentication.chromeCustomTabShow();
    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Authentication.getToken((new Authentication.VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                mainToken = Authentication.token.getAccessToken();
                code.setText("Access code:\n" + Authentication.code);
                token.setText("Access token:\n" + Authentication.token.getAccessToken());
            }
        }));

    }

}


