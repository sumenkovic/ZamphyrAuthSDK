package sumenkovic.sdk.authentication.sample;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by sumenkovic on 16.12.17..
 */

public class AuthenticatorService extends Service {
        @Override
        public IBinder onBind(Intent intent) {
            ZamphyrAccountAuthenticator authenticator = new ZamphyrAccountAuthenticator(this);
            return authenticator.getIBinder();
        }

}
